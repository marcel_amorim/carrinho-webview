# Integração com a mercafácil
base **https://testes.9bits.com.br/**
<br>

## rota:
**api/loja/accounts/token-user-mercafacil/**
### request:
parâmetro      | descrição
:--------------: | :---------:
identificador | cpf 
hash_mercafacil| código de segurança da loja (jyf2phsbrweictysxi0gki)


### response:
parâmetro      | descrição
:-------------- | :---------
session_key | hash da sessão do cliente

<br>
<br>

## rota
**api/loja/accounts/criar-usuario-mercafacil/**
### request:
parâmetro      | descrição
:-------------- | :---------
nome | string (max=80)
email | emailfield
cpf_cnpj | cpf ou cnpj (max=30)
telefone | string (max=20)
cidade | string (max=255)
uf | string (max=2)
cep | string(max=14)
bairro | string (max=255)
rua | string (max=255)
numero | string (max=30)
complemento | string (max=255)



