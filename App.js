import React from 'react';
import {StatusBar, useColorScheme} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import 'react-native-gesture-handler';

import Navigator from './src/routes/Navigator';

Icon.loadFont();

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <Navigator />
    </>
  );
};

export default App;
