import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeTabs from './HomeTabs';
import EcommerceScreen from '../screens/ecommerce';

const Stack = createStackNavigator();

const Navigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeTabs}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Ecommerce" component={EcommerceScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
