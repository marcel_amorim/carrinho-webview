import React from 'react';
import FeatherIcon from 'react-native-vector-icons/Feather';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';

import LoginScreen from '../screens/login';
import SignUpScreen from '../screens/signup';

const Tab = createBottomTabNavigator();

const HomeTabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;

          if (route.name === 'Login') {
            iconName = 'lock';
          } else {
            iconName = 'user';
          }

          // You can return any component that you like here!
          return <FeatherIcon name={iconName} size={size} color={color} />;
        },
      })}>
      <Tab.Screen name="Login" component={LoginScreen} />
      <Tab.Screen name="Registrar" component={SignUpScreen} />
    </Tab.Navigator>
  );
};

export default HomeTabs;
