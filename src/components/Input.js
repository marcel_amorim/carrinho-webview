import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

// import { Container } from './styles';

const Input = ({label, maxLength, value, onChange, half, email, number}) => {
  const props = {};

  if (email) {
    props.autoCapitalize = 'none';
    props.keyboardType = 'email-address';
  }

  if (number) {
    props.keyboardType = 'number-pad';
  }

  return (
    <View style={half ? [styles.inputHalf, styles.row] : styles.row}>
      <View style={styles.content}>
        <Text>{label}</Text>
        <View style={styles.row}>
          <TextInput
            style={styles.input}
            maxLength={maxLength}
            value={value}
            onChangeText={onChange}
            {...props}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    flex: 1,
    height: 40,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#d3d3d3',
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 5,
  },
  inputHalf: {
    flex: 0.49,
  },
  content: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default Input;
