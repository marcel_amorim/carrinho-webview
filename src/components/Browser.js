import React from 'react';
import {Dimensions, Linking, StyleSheet, Text, View} from 'react-native';
import {WebView} from 'react-native-webview';
import {Modalize} from 'react-native-modalize';

export default class Browser extends React.PureComponent {
  static _ref;

  static setRef(ref) {
    this._ref = ref;
  }

  static getRef() {
    return this._ref;
  }

  static show(request) {
    this.getRef()?.openModal(request);
  }

  static hide() {
    this.getRef()?.closeModal();
  }

  myRef;

  state = {
    title: '',
    url: '',
  };

  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  openModal = request => {
    this.setState(request, () => this.myRef.current?.open());
  };

  closeModal = () => {
    this.myRef.current?.close();
  };

  openBrowser = () => {
    Linking.openURL(this.state.url);
    this.closeModal();
  };

  render() {
    const {title, url} = this.state;

    return (
      <Modalize ref={this.myRef} scrollViewProps={{stickyHeaderIndices: [0]}}>
        <View style={styles.header}>
          <Text style={styles.title}>{title}</Text>
        </View>
        <WebView
          source={{uri: url}}
          style={styles.webview}
          allowsFullscreenVideo
        />
      </Modalize>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 40,
    flexDirection: 'row',
    backgroundColor: '#5632B6',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    textAlign: 'center',
    flex: 1,
    marginTop: 10,
    color: '#fff',
  },
  webview: {
    flex: 1,
    height: Dimensions.get('screen').height - 100,
    width: Dimensions.get('screen').width,
  },
});
