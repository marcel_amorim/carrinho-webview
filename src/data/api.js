import constants from '../util/constants';
import {onlyNumbersMask} from '../util/masks';

export const loginApi = async cpf => {
  const body = new FormData();
  body.append('identificador', onlyNumbersMask(cpf));
  body.append('hash_mercafacil', constants.HASH_MF);

  const response = fetch(
    `${constants.BASE_URL}api/accounts/token-user-mercafacil/`,
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: body,
    },
  );

  const data = await response;

  const json = await data.json();

  if (data.status > 200) {
    throw new Error(json.ERRO);
  }

  return json.session_key;
};

export const signUpApi = async formState => {
  const body = new FormData();
  body.append('hash_mercafacil', constants.HASH_MF);
  body.append('nome', formState.name);
  body.append('email', formState.email);
  body.append('cpf_cnpj', onlyNumbersMask(formState.cpf));
  body.append('telefone', onlyNumbersMask(formState.phone));
  body.append('cidade', formState.city);
  body.append('uf', formState.state);
  body.append('cep', onlyNumbersMask(formState.zipCode));
  body.append('bairro', formState.district);
  body.append('rua', formState.street);
  body.append('numero', formState.number);
  body.append('complemento', formState.complement);

  const response = fetch(
    `${constants.BASE_URL}api/accounts/criar-usuario-mercafacil/`,
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: body,
    },
  );

  const data = await response;

  const json = await data.json();

  if (data.status > 200) {
    throw new Error(json.ERRO);
  }

  return json.session_key;
};
