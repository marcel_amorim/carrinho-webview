import React, {useState} from 'react';

import {
  ActivityIndicator,
  View,
  SafeAreaView,
  StyleSheet,
  useColorScheme,
  Text,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';

import {cepMask, cpfMask, onlyNumbersMask, phoneMask} from '../../util/masks';
import constants from '../../util/constants';
import Input from '../../components/Input';
import {signUpApi} from '../../data/api';

const SignUpScreen = ({navigation}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const [loading, setLoading] = useState(false);
  const [formState, setFormState] = useState({
    name: '',
    email: '',
    cpf: '',
    phone: '',
    city: '',
    state: '',
    zipCode: '',
    district: '',
    street: '',
    number: '',
    complement: '',
  });

  const backgroundColor = {
    backgroundColor: isDarkMode ? '#1a1a1a' : '#fafafa',
  };

  const openWebview = async () => {
    try {
      setLoading(true);
      const session = await signUpApi(formState);

      navigation.navigate('Ecommerce', {
        url: constants.ECOMMERCE_URL,
        session: session,
      });
    } catch (e) {
      Alert.alert('Sign up', e.message);
    } finally {
      setLoading(false);
    }
  };

  const updateState = (field, value) => {
    setFormState(state => ({...state, [field]: value}));
  };

  return (
    <SafeAreaView style={[styles.container, backgroundColor]}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={[styles.content, backgroundColor]}>
          <View style={styles.row}>
            <Input
              label="Nome"
              value={formState.name}
              onChange={value => updateState('name', value)}
              maxLength={80}
              half
            />
            <Input
              label="Email"
              value={formState.email}
              onChange={value => updateState('email', value)}
              maxLength={255}
              half
              email
            />
          </View>
          <View style={styles.row}>
            <Input
              label="CPF"
              value={formState.cpf}
              onChange={value => updateState('cpf', cpfMask(value))}
              half
              number
            />
            <Input
              label="Telefone"
              value={formState.phone}
              onChange={value => updateState('phone', phoneMask(value))}
              half
              number
            />
          </View>
          <Input
            label="Rua"
            value={formState.street}
            onChange={value => updateState('street', value)}
            maxLength={255}
          />
          <View style={styles.row}>
            <Input
              label="Número"
              value={formState.number}
              onChange={value => updateState('number', onlyNumbersMask(value))}
              maxLength={30}
              half
              number
            />
            <Input
              label="Complemento"
              value={formState.complement}
              onChange={value => updateState('complement', value)}
              half
              maxLength={255}
            />
          </View>
          <View style={styles.row}>
            <Input
              label="CEP"
              value={formState.zipCode}
              onChange={value => updateState('zipCode', cepMask(value))}
              half
              number
            />
            <Input
              label="Bairro"
              value={formState.district}
              onChange={value => updateState('district', value)}
              half
              maxLength={255}
            />
          </View>
          <View style={styles.row}>
            <Input
              label="Cidade"
              value={formState.city}
              onChange={value => updateState('city', value)}
              half
              maxLength={255}
            />
            <Input
              label="UF"
              value={formState.state}
              onChange={value => updateState('state', value.toUpperCase())}
              half
              maxLength={2}
            />
          </View>
          {!loading && (
            <View style={styles.row}>
              <TouchableOpacity style={styles.button} onPress={openWebview}>
                <Text style={styles.buttonText}>Abrir E-commerce</Text>
              </TouchableOpacity>
            </View>
          )}
          {loading && <ActivityIndicator color="blue" size="large" />}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    flexGrow: 1,
    flexDirection: 'column',
    padding: 20,
  },
  input: {
    flex: 1,
    height: 40,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#d3d3d3',
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 5,
  },
  inputHalf: {
    flex: 0.49,
  },
  button: {
    backgroundColor: '#006400',
    height: 40,
    flex: 1,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: -1, height: 2},
    shadowRadius: 3,
    shadowOpacity: 0.2,
  },
  buttonText: {
    color: '#fff',
    fontSize: 14,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default SignUpScreen;
