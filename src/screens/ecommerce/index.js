import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {WebView} from 'react-native-webview';

const EcommerceScreen = ({route}) => {
  const {url, session} = route.params;

  return (
    <WebView
      source={{
        uri: url,
        headers: {Cookie: `sessionid=${session}`},
      }}
      style={styles.webview}
      originWhitelist={['*']}
      sharedCookiesEnabled
      javaScriptEnabled
    />
  );
};

const styles = StyleSheet.create({
  webview: {
    flex: 1,
    height: Dimensions.get('screen').height - 100,
    width: Dimensions.get('screen').width,
  },
});

export default EcommerceScreen;
