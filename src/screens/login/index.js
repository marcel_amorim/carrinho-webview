import React, {useState} from 'react';

import {
  View,
  SafeAreaView,
  StyleSheet,
  useColorScheme,
  Text,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';

import {cpfMask} from '../../util/masks';
import {loginApi} from '../../data/api';
import constants from '../../util/constants';
import Input from '../../components/Input';

const LoginScreen = ({navigation}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const [cpf, setCpf] = useState('');
  const [loading, setLoading] = useState(false);

  const backgroundColor = {
    backgroundColor: isDarkMode ? '#1a1a1a' : '#fafafa',
  };

  const openWebview = async () => {
    try {
      setLoading(true);
      const session = await loginApi(cpf);

      navigation.navigate('Ecommerce', {
        url: constants.ECOMMERCE_URL,
        session: session,
      });
    } catch (e) {
      Alert.alert('Login', e.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={[styles.safeArea, backgroundColor]}>
      <View style={[styles.content, backgroundColor]}>
        <Input
          label="CPF"
          value={cpf}
          maxLength={14}
          onChange={value => setCpf(cpfMask(value))}
        />
        {!loading && (
          <TouchableOpacity style={styles.button} onPress={openWebview}>
            <Text style={styles.buttonText}>Abrir E-commerce</Text>
          </TouchableOpacity>
        )}
        {loading && <ActivityIndicator color="blue" size="large" />}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  content: {
    flex: 1,
    flexGrow: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  input: {
    width: 200,
    height: 40,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#d3d3d3',
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 5,
  },
  button: {
    backgroundColor: '#006400',
    height: 40,
    width: 150,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: -1, height: 2},
    shadowRadius: 3,
    shadowOpacity: 0.2,
  },
  buttonText: {
    color: '#fff',
    fontSize: 14,
  },
});

export default LoginScreen;
